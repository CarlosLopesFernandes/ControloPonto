function getRegisters(){
    var register = [],
    a = {
        "nome": "José Ferreira",
        "inDay": "10/04/2018",
        "outDay": "10/04/2018",
        "inHr": "09:10",
        "outHr": "17:10"
    }, 
    b = {
        "nome": "Manuel Rodrigues",
        "inDay": "10/04/2018",
        "outDay": "10/04/2018",
        "inHr": "13:00",
        "outHr": "21:00"
    },
    c = {
        "nome": "Joel Costa",
        "inDay": "10/04/2018",
        "outDay": "10/04/2018",
        "inHr": "12:00",
        "outHr": "20:00"
    },
    d = {
        "nome": "José Ferreira",
        "inDay": "09/04/2018",
        "outDay": "09/04/2018",
        "inHr": "15:30",
        "outHr": "23:30"
    },
    e = {
        "nome": "Manuel Rodrigues",
        "inDay": "09/04/2018",
        "outDay": "09/04/2018",
        "inHr": "09:00",
        "outHr": "17:00"
    },
    f = {
        "nome": "Joel Costa",
        "inDay": "09/04/2018",
        "outDay": "09/04/2018",
        "inHr": "14:00",
        "outHr": "22:00"
    },
    g = {
        "nome": "Manuel Rodrigues",
        "inDay": "08/04/2018",
        "outDay": "08/04/2018",
        "inHr": "07:00",
        "outHr": "15:00"
    },
    h = {
        "nome": "Joel Costa",
        "inDay": "08/04/2018",
        "outDay": "08/04/2018",
        "inHr": "09:10",
        "outHr": "17:10"
    };
    register.push(a);
    register.push(b);
    register.push(c);
    register.push(d);
    register.push(e);
    register.push(f);
    register.push(g);
    register.push(h);
return register;
}

function getEmployees(){
    emp = ["José Ferreira", "Joel Costa", "Manuel Rodrigues"];
return emp;
}