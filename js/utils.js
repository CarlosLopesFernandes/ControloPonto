function handleRegisterTable(data){
    var counter = 0;
    var table = '<table id="register-table" class="table table-striped">' + 
                '<thead>' +
                    '<tr>' +
                        '<th>Funcionário</th>' +
                        '<th>Data Entrada</th>'+
                        '<th>Data Saída</th>' +
                        '<th>Hora Entrada</th>' +
                        '<th>Hora Saída</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody>';

    for(var i = 0; i < data.length; i++){
        var r = data[i];
        table += "<tr for='" + counter + "'>" + 
                    "<td type='name' id='name-"+counter + "'>" + r.nome + "</td>" + 
                    "<td type='inD' id='dayIn-"+counter + "'>" + r.inDay + "</td>" + 
                    "<td type='outD' id='dayOut-"+counter + "'>" + r.outDay + "</td>" + 
                    "<td type='inH' id='hrIn-"+counter + "'>" + r.inHr + "</td>" + 
                    "<td type='outH' id='hrOut-"+counter + "'>" + r.outHr + "</td>" + 
                "</tr>";
            counter++;
    }             
    table+= '</tbody>' + 
            '</table>';
return table; 
}

function handleSelectOptions(employees){
    var counter = 0;
    var select = "<select onchange='filter()' id='emp-select-option'>" + 
                    "<option value='-1' selected disabled hidden>Escolha aqui</option>" + 
                    "<option value='-1'>-- Remover Filtro --</option>";
    for(var i = 0; i<employees.length; i++){
        select += "<option value='"+counter+"'>" +  employees[i] + "</option>";
        counter++;
    }
    return select; 
}

function filterEmployee(){
    var optionValue = $('#emp-select-option').find(":selected").val(),
        name = $('#emp-select-option').find(":selected").text();
        counter = 0;
    if(optionValue != -1){
        $("#register-table tbody tr").each(function(){
            var nameII = $(this).find("td:first").text();
            if(nameII != name){
                this.remove();
            }
        });
    } 
}

function filterDataIn(){
    var minDayIn = $("#data-inferior-entrada").val(),
    maxDayIn = $("#data-superior-entrada").val();

    if(minDayIn != "" && maxDayIn !=""){
        console.log("Data Entrada ambos não nulos");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "inD"){
                    var x = $(this);
                    var x = $(this).text();
                    if(!(x >= minDayIn && x <= maxDayIn)){
                        k.remove();
                    }
                }
            });
        });

    }else if(minDayIn == "" && maxDayIn ==""){
        console.log("Data Entrada ambos nulos");
    }else if(maxDayIn ==""){
        console.log("Data Entrada máximo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "inD"){
                    var x = $(this).text();
                    if(!(x >= minDayIn)){
                        k.remove();
                    }
                }
            });
        });

    }else if(minDayIn == ""){
        console.log("Data Entrada minimo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "inD"){
                    var x = $(this).text();
                    if(!(x <= maxDayIn)){
                        k.remove();
                    }
                }
            });
        });
    }
}

function filterDataOut(){
    var minDayOut = $("#data-inferior-saída").val(),
    maxDayOut = $("#data-superior-saída").val();

    if(minDayOut != "" && maxDayOut !=""){
        console.log("Data Saída ambos não nulos");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "outD"){
                    var x = $(this).text();
                    if(!(x >= minDayOut && x <= maxDayOut)){
                        k.remove();
                    }
                }
            });
        });

    }else if(minDayOut == "" && maxDayOut ==""){
        console.log("Data Saída ambos nulos");
    }else if(maxDayOut ==""){
        console.log("Data Saída maximo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "outD"){
                    var x = $(this).text();
                    if(!(x >= minDayOut)){
                        k.remove();
                    }
                }
            });
        });

    }else if(minDayOut == ""){
        console.log("Data Saída minimo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "outD"){
                    var x = $(this).text();
                    if(!(x <= minDayIn)){
                        k.remove();
                    }
                }
            });
        });
    }
}

function filterHourIn(){
    var minHourIn = $("#hora-inferior-entrada").val(),
    maxHourIn = $("#hora-superior-entrada").val();

    if(minHourIn != "" && maxHourIn !=""){
        console.log("Hora Entrada ambos não nulos");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "inH"){
                    var x = $(this).text();
                    if(!(x >= minHourIn && x <= maxHourIn)){
                        k.remove();
                    }
                }
            });
        });
    }else if(minHourIn == "" && maxHourIn ==""){
        console.log("Hora entrada ambos nulos");
    }else if(maxHourIn ==""){
        console.log("Hora Entrada máximo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "inH"){
                    var x = $(this).text();
                    if(!(x >= minHourIn)){
                        k.remove();
                    }
                }
            });
        });
    }else if(minHourIn == ""){
        console.log("Hora Entrada minimo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "inH"){
                    var x = $(this).text();
                    if(!(x <= maxHourIn)){
                        k.remove();
                    }
                }
            });
        });
    }
}

function filterHourOut(){
    var minHourOut = $("#hora-inferior-saída").val(),
    maxHourOut = $("#hora-superior-saída").val();

    if(minHourOut != "" && maxHourOut !=""){
        console.log("Hora saída ambos não nulos");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "outH"){
                    var x = $(this).text();
                    if(!(x >= minHourOut && x <= maxHourOut)){
                        k.remove();
                    }
                }
            });
        });

    }else if(minHourOut == "" && maxHourOut ==""){
        console.log("Hora saída ambos nulos");
    }else if(maxHourOut ==""){
        console.log("Hora saída máximo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "outH"){
                    var x = $(this).text();
                    if(!(x >= minHourOut)){
                        k.remove();
                    }
                }
            });
        });

    }else if(minHourOut == ""){
        console.log("Hora saída minimo nulo");
        $("#register-table tbody tr").each(function(){
            var k = $(this);
            k.find("td").each(function(){
                if($(this).attr("type") == "outH"){
                    var x = $(this).text();
                    if(!(x <= maxHourOut)){
                        k.remove();
                    }
                }
            });
        });

    }
}

function filter(){
    $("#d-table").empty();
    $("#d-table").append(table);
    filterEmployee();
    filterDataIn();
    filterDataOut();
    filterHourIn();
    filterHourOut();
}
